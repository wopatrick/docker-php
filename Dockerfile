FROM php:7.4-fpm-alpine3.13 as app 

WORKDIR /var/www/app

RUN apk update \
    && apk add git \
    supervisor \
    bash \
    nginx \
    curl \
    libbz2 \
    libzip \
    zlib \
    libpng \
    libpng-dev \
    libjpeg-turbo-dev \
    libwebp-dev \
    libzip-dev \
    zlib-dev \
    zip \
    unzip \
#docker install extension    
    && docker-php-ext-install bcmath \
    mysqli \
    opcache \
    pdo \
    pdo_mysql \
    gd \
    zip \
    && rm -rf /var/cache/apk/*
#configure composer 
COPY --from=composer /usr/bin/composer /usr/bin/composer

COPY ./deploy/supervisord.conf /etc/supervisord.conf

#setup nginx configuration
COPY ./deploy/nginx-site.conf /etc/nginx/http.d/default.conf

#setup php-fpm configuration
COPY ./deploy/php-fpm.d/ /usr/local/etc/php-fpm.d

COPY ./deploy/entrypoint.sh /etc/entrypoint.sh

COPY . .

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" \
    #setup nginx
    && mkdir -p /run/nginx \
    && chmod 755 /etc/nginx/http.d/default.conf \
    #setup laravel
    && composer install --ignore-platform-reqs --prefer-dist --no-scripts -q -o  \
    && cp .env.ecs.dev .env \
    && chmod -R 777 storage \
    && php artisan optimize \
    && chown -R www-data:www-data /var/www \
    &&  chmod +x /etc/entrypoint.sh

EXPOSE 80

ENTRYPOINT ["/etc/entrypoint.sh"]