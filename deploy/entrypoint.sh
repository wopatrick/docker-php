#!/bin/sh
set -e

echo "* * * * * cd /var/www/app && /usr/local/bin/php artisan schedule:run >> /dev/null" >> /etc/crontabs/root
/usr/bin/supervisord -c /etc/supervisord.conf